Page({
    data: {
        list: [ {
            name: "五险一金个税计算器",
            id: "wx8a74b204c455d560",
            icon: "../../images/5x.png"
        }, {
            name: "房贷计算器",
            id: "wx3dda643da179e9fb",
            icon: "../../images/fd.jpg"
        }, {
            name: "上下标生成器",
            id: "wxa06c7a97c31e9b7a",
            icon: "../../images/sb1.png"
        }, {
            name: "更多敬请期待",
            id: "wx123456712348",
            icon: "../../images/more.png"
        } ]
    },
    onLoad: function(n) {},
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {},
    onReachBottom: function() {},
    onShareAppMessage: function() {}
});