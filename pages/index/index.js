getApp();
var sliderWidth = 100; // 需要设置slider的宽度，用于计算中间位置
Page({
    data: {
      page1: 'block',
      page2: 'none',
      color1: '#333',
      color2: '#333',
      tabs: ["宁国", "宣城"],
      activeIndex: 0,
      sliderOffset: 0,
      sliderLeft: 0
    },
    ngPart: function (e) {
      this.setData({
        page1: 'block',
        page2: 'none',
        color1: '#5495E6',
        color2: '#333'
      });
    },
    xcPart: function (e) {
      this.setData({
        page1: 'none',
        page2: 'block',
        color1: '#333',
        color2: '#5495E6'
      });
    },
    onShareAppMessage: function(e) {
        return e.from, {
            title: "宁国宣城至上海大巴时间查询",
            path: "/pages/index/index"
        };
    },
    bindReward: function(e) {
        wx.previewImage({
          current: [ "http://www.liangmann.com/images/zs.png" ],
          urls: [ "http://www.liangmann.com/images/zs.png" ]
        });
    },
    hutaiLoad: function() {
        wx.openLocation({
            latitude: 31.25875,
            longitude: 121.44994,
            name: "上海长途汽车站(沪太路中山北路1015号)"
        });
    },
    ngStation: function() {
        wx.openLocation({
            latitude: 30.64789,
            longitude: 119.00001,
            name: "宁国汽车站"
        });
    },
    oldNorthStation: function() {
        wx.openLocation({
            latitude: 31.25445,
            longitude: 121.47465,
            name: "老北站"
        });
    },
    hqpWblStation: function() {
        wx.openLocation({
            latitude: 31.1721713761,
            longitude: 121.3404107094,
            name: "沪青平公路吴宝路公交站台"
        });
    },
    qpsfzStation: function() {
        wx.openLocation({
            latitude: 31.1294988291,
            longitude: 121.1260324717,
            name: "青浦城区收费站出口青松路外青松公路公交站"
        });
    },
    callme1: function() {
        wx.makePhoneCall({
            phoneNumber: "13524175770"
        });
    },
    callme2: function() {
        wx.makePhoneCall({
            phoneNumber: "13391335850"
        });
    },
    callme3: function() {
        wx.makePhoneCall({
            phoneNumber: "13681714538"
        });
    },
    callme4: function() {
        wx.makePhoneCall({
            phoneNumber: "13485923988"
        });
    },
    callme5: function() {
        wx.makePhoneCall({
            phoneNumber: "15201935059"
        });
    },
    callme6: function() {
        wx.makePhoneCall({
            phoneNumber: "13705635536"
        });
    },
    callme7: function() {
        wx.makePhoneCall({
            phoneNumber: "13917842208"
        });
    },
    callme8: function() {
        wx.makePhoneCall({
            phoneNumber: "13816232199"
        });
    },
    callme9: function() {
        wx.makePhoneCall({
            phoneNumber: "13370011258"
        });
    },
    callme10: function() {
        wx.makePhoneCall({
            phoneNumber: "13661976866"
        });
    },
    callme11: function() {
        wx.makePhoneCall({
            phoneNumber: "13524175770"
        });
    },
    callme12: function() {
        wx.makePhoneCall({
            phoneNumber: "13965406188"
        });
    },
    callme13: function() {
        wx.makePhoneCall({
            phoneNumber: "15856330606"
        });
    },
    callme14: function() {
        wx.makePhoneCall({
            phoneNumber: "13370011258"
        });
    },
    callme15: function() {
        wx.makePhoneCall({
            phoneNumber: "13681714538"
        });
    },
    callme16: function() {
        wx.makePhoneCall({
            phoneNumber: "13965406188"
        });
    },
    callme17: function() {
        wx.makePhoneCall({
            phoneNumber: "15856330606"
        });
    },
    callme18: function() {
        wx.makePhoneCall({
            phoneNumber: "13865392022"
        });
    },
    callme19: function() {
        wx.makePhoneCall({
            phoneNumber: "13966213398"
        });
    },
    callme20: function() {
        wx.makePhoneCall({
            phoneNumber: "13956564322"
        });
    },
    
    callmexc1: function() {
        wx.makePhoneCall({
            phoneNumber: "13956595836"
        });
    },
    callmexc2: function() {
        wx.makePhoneCall({
            phoneNumber: "13166223555"
        });
    },
    callmexc3: function() {
        wx.makePhoneCall({
            phoneNumber: "13966201071"
        });
    },
    callmexc4: function () {
      wx.makePhoneCall({
          phoneNumber: "13764680988"
      });
    },
    callmexc5: function () {
      wx.makePhoneCall({
          phoneNumber: "13013138785"
      });
    },
    callmexc6: function () {
      wx.makePhoneCall({
          phoneNumber: "13761157657"
      });
    },
    callmexc7: function () {
      wx.makePhoneCall({
          phoneNumber: "13916060789"
      });
    },
    callmexc8: function () {
      wx.makePhoneCall({
          phoneNumber: "13966227619"
      });
    },
    callmexc9: function () {
      wx.makePhoneCall({
          phoneNumber: "13966201073"
      });
    },
    callmexc10: function () {
      wx.makePhoneCall({
          phoneNumber: "13046666527"
      });
    },
    callmexc11: function () {
      wx.makePhoneCall({
          phoneNumber: "13761953882"
      });
    },
    callmexc12: function () {
      wx.makePhoneCall({
          phoneNumber: "13856304505"
      });
    },
    callmexc13: function () {
      wx.makePhoneCall({
          phoneNumber: "13966201072"
      });
    },
    callmexc14: function () {
      wx.makePhoneCall({
          phoneNumber: "18616391613"
      });
    },
    callmexc15: function () {
      wx.makePhoneCall({
          phoneNumber: "13965667218"
      });
    },
    callmexc16: function () {
      wx.makePhoneCall({
          phoneNumber: "13564672678"
      });
    },
    callmexc17: function () {
      wx.makePhoneCall({
          phoneNumber: "13916575812"
      });
    },
    callmexc18: function () {
      wx.makePhoneCall({
          phoneNumber: "13705631830"
      });
    },
    callmexc19: function () {
      wx.makePhoneCall({
          phoneNumber: "13705636096"
      });
    },
    callmexc20: function () {
      wx.makePhoneCall({
          phoneNumber: "13856331511"
      });
    },
    callmexc21: function () {
      wx.makePhoneCall({
          phoneNumber: "13818711151"
      });
    },
    callmexc22: function () {
      wx.makePhoneCall({
          phoneNumber: "13761912001"
      });
    },
    callmexc23: function () {
      wx.makePhoneCall({
          phoneNumber: "13328288800"
      });
    },
    callmexc24: function () {
      wx.makePhoneCall({
          phoneNumber: "15921137782"
      }); 
    },
    callmexc25: function () {
      wx.makePhoneCall({
          phoneNumber: "13705637947"
      });
    },
    callmexc26: function () {
      wx.makePhoneCall({
          phoneNumber: "13611664397"
      });
    },
    callmexc27: function () {
      wx.makePhoneCall({
          phoneNumber: "13013136677"
      });
    },
    callmexc28: function () {
      wx.makePhoneCall({
          phoneNumber: "13205637088"
      });
    },
    callmexc29: function () {
      wx.makePhoneCall({
          phoneNumber: "15155587088"
      });
    },
    callmexc30: function () {
      wx.makePhoneCall({
          phoneNumber: "13955389666"
      });
    },
    callmexc31: function () {
      wx.makePhoneCall({
          phoneNumber: "13402005979"
      });
    },
    callmexc32: function () {
      wx.makePhoneCall({
          phoneNumber: "13865387757"
      });
    },
    callmexc33: function () {
      wx.makePhoneCall({
          phoneNumber: "15026854409"
      });
    },
    callmexc34: function () {
      wx.makePhoneCall({
          phoneNumber: "13705638607"
      });
    },
    callmexc35: function () {
      wx.makePhoneCall({
          phoneNumber: "13402005979"
      });
    },
    callmexc36: function () {
      wx.makePhoneCall({
          phoneNumber: "13705636069"
      });
    },
    callmexc37: function () {
      wx.makePhoneCall({
          phoneNumber: "13966201073"
      });
    },
    callmexc38: function () {
      wx.makePhoneCall({
          phoneNumber: "13046666527"
      });
    },
    callmexc39: function () {
      wx.makePhoneCall({
          phoneNumber: "13621887252"
      });
    },
    callmexc40: function () {
      wx.makePhoneCall({
          phoneNumber: "13966201072"
      });
    },
    callmexc41: function () {
      wx.makePhoneCall({
          phoneNumber: "18616391613"
      });
    },
    callmexc42: function () {
      wx.makePhoneCall({
          phoneNumber: "13965667218"
      });
    },
    callmexc43: function () {
      wx.makePhoneCall({
          phoneNumber: "13564672678"
      });
    },
    callmexc44: function () {
      wx.makePhoneCall({
          phoneNumber: "13966217982"
      });
    },
    callmexc45: function () {
      wx.makePhoneCall({
          phoneNumber: "13124816780"
      });
    },
    callmexc46: function () {
      wx.makePhoneCall({
          phoneNumber: "13053240927"
      });
    },
    callmexc47: function () {
      wx.makePhoneCall({
          phoneNumber: "13817268819"
      });
    },
    callmexc48: function () {
      wx.makePhoneCall({
          phoneNumber: "13856331511"
      });
    },
    callmexc49: function () {
      wx.makePhoneCall({
          phoneNumber: "13818711151"
      });
    },
    callmexc50: function () {
      wx.makePhoneCall({
          phoneNumber: "13482887588"
      });
    },
    callmexc51: function () {
      wx.makePhoneCall({
          phoneNumber: "13966227619"
      });
    },
    callmexc52: function () {
      wx.makePhoneCall({
          phoneNumber: "13916060789"
      });
    },
    callmexc53: function () {
      wx.makePhoneCall({
          phoneNumber: "13856304505"
      });
    },
    callmexc54: function () {
      wx.makePhoneCall({
          phoneNumber: "13761953882"
      });
    },
    callmexc55: function () {
      wx.makePhoneCall({
          phoneNumber: "13013138785"
      });
    },
    callmexc56: function () {
      wx.makePhoneCall({
          phoneNumber: "13761157657"
      });
    },
    callmexc57: function () {
      wx.makePhoneCall({
          phoneNumber: "13524110907"
      });
    },
    callmexc58: function () {
      wx.makePhoneCall({
          phoneNumber: "13865344777"
      });
    },
    callmexc59: function () {
      wx.makePhoneCall({
          phoneNumber: "13585888286"
      });
    },
    callmexc60: function () {
      wx.makePhoneCall({
          phoneNumber: "15205634819"
      });
    },
    callmexc61: function () {
      wx.makePhoneCall({
          phoneNumber: "13816410982"
      });
    },
    callmexc62: function () {
      wx.makePhoneCall({
          phoneNumber: "13482850803"
      });
    },
    callmexc63: function () {
      wx.makePhoneCall({
          phoneNumber: "13641772005"
      });
    },
    callmexc64: function () {
      wx.makePhoneCall({
          phoneNumber: "13701714205"
      });
    },
    callmexc65: function () {
      wx.makePhoneCall({
          phoneNumber: "13637211550"
      });
    },
    callmexc66: function () {
      wx.makePhoneCall({
          phoneNumber: "13816410982"
      });
    },
    callmexc67: function () {
      wx.makePhoneCall({
          phoneNumber: "13482850830"
      });
    },
    callmexc68: function () {
      wx.makePhoneCall({
          phoneNumber: "13641772005"
      });
    },
    callmexc69: function () {
      wx.makePhoneCall({
          phoneNumber: "13916165120"
      });
    },
    callmexc70: function () {
      wx.makePhoneCall({
          phoneNumber: "13865387757"
      });
    },
    callmexc71: function () {
      wx.makePhoneCall({
          phoneNumber: "15026854402"
      });
    },
    callmexc72: function () {
      wx.makePhoneCall({
          phoneNumber: "15026854409"
      });
    },
    callmexc73: function () {
      wx.makePhoneCall({
          phoneNumber: "13661752111"
      });
    },
    callmexc74: function () {
      wx.makePhoneCall({
          phoneNumber: "13329131111"
      });
    },
    callmexc75: function () {
      wx.makePhoneCall({
          phoneNumber: "13856333539"
      });
    },
    onLoad: function() {
      var that = this;
      wx.getSystemInfo({
        success: function (res) {
          console.log('res', res)
          that.setData({
            sliderLeft: (res.windowWidth / that.data.tabs.length - sliderWidth) / 2,
            sliderOffset: res.windowWidth / that.data.tabs.length * that.data.activeIndex
          });
        }
      });
    },
    tabClick: function (e) {
      this.setData({
        sliderOffset: e.currentTarget.offsetLeft,
        activeIndex: e.currentTarget.id
      });
    },
});